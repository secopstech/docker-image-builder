# Centos based minimal docker image builder script.

Forked from https://github.com/moby/moby/blob/master/contrib/mkimage-yum.sh
and includes some minor customizations.

If you don't pass any additional packages via -p argument it will only install basesystem, coreutils, shadow-utils.

